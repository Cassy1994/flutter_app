import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/providers/app_bar_provider.dart';
import 'package:flutter_app/router.dart';
import 'package:flutter_app/screens/posts_screen.dart';
import 'package:flutter_app/screens/user_screen.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';

class MainScreen extends StatelessWidget {
  Widget appBar(BuildContext context) {
    return PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          title: Text('app'.tr()),
          automaticallyImplyLeading: true,
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: appBar(context),
        body:  _MainScreen()
      ),
    );
  }
}

class _MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<_MainScreen> {
  bool clickedCentreFAB = false;
  int _currentIndex = 0;

  final List<StatefulWidget> _pages = [
    PostsScreen(),
    UserScreen()
  ];

  updateTitle(String title) => context.read<AppBarProvider>().title = title;

  void updateTabSelection(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        body: Center(
          child: _pages[_currentIndex]
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked, //specify the location of the FAB
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, AppRouter.newPost).then((value) => null);
          },
          child: Container(
            margin: EdgeInsets.all(15.0),
            child: Icon(Icons.add),
          ),
          elevation: 4.0,
        ),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            margin: EdgeInsets.only(left: 12.0, right: 12.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                IconButton(
                  onPressed: () => updateTabSelection(0),
                  iconSize: 27.0,
                  icon: Icon(
                    Icons.home,
                    color: _currentIndex == 0
                        ? Colors.blue
                        : Colors.grey.shade400,
                  ),
                ),
                SizedBox(
                  width: 50.0,
                ),
                IconButton(
                  onPressed: () => updateTabSelection(1),
                  iconSize: 27.0,
                  icon: Icon(
                    Icons.person,
                    color: _currentIndex == 1
                        ? Colors.blue
                        : Colors.grey.shade400,
                  ),
                ),
              ],
            ),
          ),
          shape: CircularNotchedRectangle(),
          color: Colors.white,
        ),
      )
    );
  }
}