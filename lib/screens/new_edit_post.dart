import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/models/Post.dart';
import 'package:flutter_app/providers/app_bar_provider.dart';
import 'package:flutter_app/redux/posts/post_actions.dart';
import 'package:flutter_app/redux/posts/post_states.dart';
import 'package:flutter_app/redux/state.dart';
import 'package:flutter_app/redux/store.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:provider/provider.dart';

class NewEditPostScreen extends StatelessWidget {
  Widget appBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(50.0),
      child: Consumer<AppBarProvider>(
        builder: (_, AppBarProvider model, Widget child) {
          return AppBar(
            title: Text(model.title != null ? model.title : ""),
            automaticallyImplyLeading: true,
          );
        }
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: appBar(context),
        body:  _NewPostScreen()
      ),
    );
  }
}

class _NewPostScreen extends StatefulWidget {
  @override
  _NewPostState createState() => _NewPostState();
}

class _NewPostState extends State<_NewPostScreen> {
  String _title = "",
      _body = "";

  @override
  Widget build(BuildContext context) {
    final Post post = ModalRoute.of(context).settings.arguments;

    WidgetsBinding.instance.addPostFrameCallback((_) =>
            context.read<AppBarProvider>().title = post != null ? 'modify_post'.tr() : 'new_post'.tr());

    if (post != null) {
      _title = post.title;
      _body = post.body;
    }

    return new StoreConnector<AppState, PostsState>(
      distinct: true,
      converter: (store) => store.state.postsState,
      onDidChange: (PostsState state) {
        if (state.isNew || state.isEdited) Navigator.pop(context);
      },
      builder: (context, state) {
        return Scaffold(
          body: new ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  controller: TextEditingController(text: _title),
                  autocorrect: false,
                  onChanged: (String title) => _title = title,
                  decoration: InputDecoration(
                    hintText: 'Titolo del post',
                    filled: true,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  controller: TextEditingController(text: _body),
                  minLines: 10,
                  maxLines: 15,
                  autocorrect: false,
                  onChanged: (String body) => _body = body,
                  decoration: InputDecoration(
                    hintText: 'Corpo del post',
                    filled: true,
                  ),
                ),
              ),
              Container(
                height: 50,
                padding: EdgeInsets.fromLTRB(50, 0, 50, 0),
                child: RaisedButton(
                  onPressed: () {
                    if (_title.isNotEmpty || _body.isNotEmpty) {
                      if (post == null) {
                        ReduxStore.store.dispatch(NewPostAction(_title, _body));
                      }
                      else {
                        ReduxStore.store.dispatch(EditPostAction(post.id, _title, _body));
                      }
                    }
                  },
                  color: Colors.lightBlueAccent,
                  child: Text(
                    post == null ? 'Pubblica' : 'Modifica',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16
                    )
                  ),
                ),
              ),
            ],
          )
        );
      }
    );
  }
}