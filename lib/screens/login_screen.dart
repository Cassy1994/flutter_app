import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/redux/common/common_actions.dart';
import 'package:flutter_app/redux/common/common_states.dart';
import 'package:flutter_app/redux/store.dart';
import 'package:flutter_app/redux/users/user_actions.dart';
import 'package:easy_localization/easy_localization.dart';

class LoginScreen extends StatelessWidget {
  Widget appBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(50.0),
      child: AppBar(
        title: Text('login'.tr()),
        automaticallyImplyLeading: true,
        backgroundColor: Colors.blue,
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body:  _LoginScreen()
      ),
    );
  }
}

class _LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<_LoginScreen> {
  String _email;
  String _password;
  bool _obscureText = true;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        ReduxStore.store.dispatch(UpdateAppBarAction(CommonStates(appBarTitle: 'login'.tr()))));
  }

  @override
  Widget build(BuildContext context) {
    final _userState = ReduxStore.store.state.usersState;

    return Form(
      key: _formKey,
      child: ListView(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 5,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.blue,
                  Colors.blue
                ],
              ),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(90)
              )
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Spacer(),
                Align(
                  alignment: Alignment.center,
                  child: Icon(Icons.person,
                    size: 90,
                    color: Colors.white,
                  ),
                ),
                Spacer(),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: const EdgeInsets.only(
                      bottom: 32,
                      right: 32
                    ),
                    child: Text('Benvenuto, entra pure',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Container(
            padding: EdgeInsets.fromLTRB(10, 0, 50, 10),
            child: TextFormField(
              decoration: InputDecoration(
                icon: Icon(Icons.accessibility),
                border: OutlineInputBorder(),
                labelText: 'Email',
              ),
              onChanged: (String value) {
                _email = value;
              },
              validator: (value) {
                if (value.isEmpty) {
                  return 'Inserisci email';
                }
                return null;
              },
            ),
          ),
          SizedBox(height: 10),
          Container(
            padding: EdgeInsets.fromLTRB(10, 0, 50, 0),
            child: TextFormField(
              decoration: InputDecoration(
                icon: Icon(Icons.person),
                border: OutlineInputBorder(),
                labelText: 'Password',
                suffixIcon: GestureDetector(
                  onTap: () {
                    setState(() {
                      _obscureText = !_obscureText;
                    });
                  },
                  child: Icon(
                    _obscureText ? Icons.visibility : Icons.visibility_off,
                  ),
                ),
              ),
              obscureText: _obscureText,
              onChanged: (String value) {
                _password = value;
              },
              validator: (value) {
                if (value.isEmpty) {
                  return 'Inserisci password';
                }
                return null;
              },
            ),
          ),
          SizedBox(height: 20),
          Container(
            height: 50,
            padding: EdgeInsets.fromLTRB(50, 0, 50, 0),
            child: RaisedButton(
              textColor: Colors.white,
              color: Colors.blue,
              child: Text('Entra'),
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _doLogin();
                }
              },
            )
          ),
          SizedBox(height: 10),
          Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(50, 20, 50, 20),
                child: Visibility(
                  child: CircularProgressIndicator(),
                  visible: _userState.isLoading,
                ),
              ),
            ],
          ),
          SizedBox(height: 10),
          Text(
            _userState.isError ? _userState.errorMessage : "",
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red)
          ),
          SizedBox(height: 20),
          Padding(
            padding: EdgeInsets.fromLTRB(50, 20, 50, 20),
            child: Text(
              'login_message'.tr(),
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.blueGrey)
            )
          )
        ]
      )
    );
  }

  void _doLogin() {
    ReduxStore.store.dispatch(LoginAction(_email, _password));
  }
}