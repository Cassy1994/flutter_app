import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/models/Comment.dart';
import 'package:flutter_app/models/Post.dart';
import 'package:flutter_app/providers/app_bar_provider.dart';
import 'package:flutter_app/redux/posts/post_actions.dart';
import 'package:flutter_app/redux/posts/post_states.dart';
import 'package:flutter_app/redux/state.dart';
import 'package:flutter_app/redux/store.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:provider/provider.dart';

class PostDetailsScreen extends StatelessWidget {
  Widget appBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(50.0),
      child: Consumer<AppBarProvider>(
          builder: (_, AppBarProvider model, Widget child) {
            return AppBar(
              title: Text(model.title != null ? model.title : ""),
              automaticallyImplyLeading: true,
            );
          }
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: appBar(context),
          body:  _PostDetailsScreen()
      ),
    );
  }
}

class _PostDetailsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PostsDetailsState();
}

class _PostsDetailsState extends State<_PostDetailsScreen> {

  final _commentTextController = TextEditingController();
  final FocusNode _focusNodeComment = FocusNode();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
        Post post = ModalRoute.of(context).settings.arguments;
        context.read<AppBarProvider>().title = post.title;
        ReduxStore.store.dispatch(GetPostAction(post.id));
    });

    _commentTextController.addListener(() {
      final text = _commentTextController.text;
      _commentTextController.value = _commentTextController.value.copyWith(
        text: text,
        selection:
        TextSelection(baseOffset: text.length, extentOffset: text.length),
        composing: TextRange.empty,
      );
    });
  }

  void _newComment(Post post) {
    ReduxStore.store.dispatch(NewCommentAction(post, Comment(_commentTextController.text)));
    _commentTextController.text = "";
  }

  void _editComment(Post post) {
    ReduxStore.store.dispatch(EditCommentAction(post));
  }

  void _onLikeComment(Post post, int index) =>
      ReduxStore.store.dispatch(LikeCommentAction(post, index));

  void _deleteComment(Post post, int index) =>
      ReduxStore.store.dispatch(DeleteCommentAction(post, index));

  Widget _openCommentMenu(Post post, int index) => PopupMenuButton<int>(
    itemBuilder: (context) => [
      PopupMenuItem(
        value: 1,
        child: Text("Modifica"),
      ),
      PopupMenuItem(
        value: 2,
        child: Text("Elimina"),
      ),
    ],
    onSelected: (value) {
      switch (value) {
        case 1:
          _editCommentDialog(post, index);
          break;
        case 2:
          _deleteComment(post, index);
          break;
      }
    },
  );

  _editCommentDialog(Post post, int index) {
    _focusNodeComment.requestFocus();
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Modifica commento'),
          content: TextField(
            controller: TextEditingController()..text = post.comments[index].body,
            focusNode: _focusNodeComment,
            onChanged: (String text) => { post.comments[index].body = text },
          ),
          actions: [
            FlatButton(
              textColor: Color(0xFF000000),
              onPressed: () { Navigator.pop(context); },
              child: Text('Chiudi'),
            ),
            FlatButton(
              textColor: Color(0xFF007DFF),
              onPressed: () {
                _editComment(post);
                Navigator.pop(context);
              },
              child: Text('Modifica'),
            ),
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, PostsState>(
      distinct: true,
      converter: (store) => store.state.postsState,
      builder: (context, state) {
        return Scaffold(
          body: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                child: Text(
                  state.post.body,
                  style: TextStyle(
                    fontSize: 15
                  ),
                )
              ),
              if (state.post.comments != null) Row(
                children: <Widget>[
                  Expanded(
                    child: ListView.builder(
                      padding: const EdgeInsets.all(10.0),
                      shrinkWrap: true,
                      itemCount: state.post.comments.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                          color: Color(0xFFD1E6FA),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(state.post.comments[index].body),
                              ),
                              Column(
                                children: <Widget>[
                                  IconButton(
                                    icon: Icon(
                                      Icons.favorite,
                                      color: state.post.comments[index]
                                          .isLiked ? Colors.red : Colors
                                          .grey,
                                      size: 18.0,
                                    ),
                                    onPressed: () => _onLikeComment(state.post, index),
                                  ),
                                ]
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: _openCommentMenu(state.post, index)
                              ),
                            ]
                          ),
                        );
                      }
                    ),
                  ),
                ]
              ),
              Container(
                margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                child: Row(
                  children: [
                    Flexible(
                      child: TextField(
                        controller: _commentTextController,
                        decoration: InputDecoration(
                          hintText: "Scrivi un commento",
                          prefixIcon: Icon(Icons.comment),
                        ),
                      ),
                    ),
                    Container(
                      child: IconButton(
                        icon: const Icon(Icons.send, size: 18),
                        onPressed: () => _newComment(state.post),
                      ),
                    )
                  ],
                ),
              ),
            ],
          )
        );
      }
    );
  }
}