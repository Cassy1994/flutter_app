import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/redux/state.dart';
import 'package:flutter_app/redux/store.dart';
import 'package:flutter_app/redux/users/user_actions.dart';
import 'package:flutter_redux/flutter_redux.dart';

class UserScreen extends StatefulWidget {
  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  AppState state;

  String _email, _displayName;

  void updateUserInfo() =>
      ReduxStore.store.dispatch(UpdateUserInfoAction(_email, _displayName, "phoneNumber"));

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      distinct: true,
      converter: (store) => store.state,
      builder: (context, state) {
        return Scaffold(
          body: ListView(
            padding: const EdgeInsets.all(8),
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    color: Colors.blue,
                    child: Container(
                      width: double.infinity,
                      height: 250.0,
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              backgroundImage: AssetImage("assets/images/francesco.jpeg"),
                              radius: 50.0,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              state.usersState.user.email,
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.white,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Card(
                              margin: EdgeInsets.symmetric(horizontal: 10.0,vertical: 5.0),
                              clipBehavior: Clip.antiAlias,
                              color: Colors.white,
                              elevation: 5.0,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 10.0),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Column(
                                        children: <Widget>[
                                          Text(
                                            "Post Pubblicati: ${state.postsState.posts.length}",
                                            style: TextStyle(
                                              color: Colors.redAccent,
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 5.0,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4.0,vertical: 10.0),
                    child: TextField(
                      controller: TextEditingController(text: state.usersState.user.email),
                      autocorrect: false,
                      onChanged: (String email) => _email = email,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4.0,vertical: 10.0),
                    child: TextField(
                      autocorrect: false,
                      onChanged: (String displayName) => _displayName = displayName,
                      decoration: InputDecoration(
                        hintText: "Nome e cognome"
                      ),
                    ),
                  ),Container(
                    width: 300.00,
                    child: RaisedButton(
                      onPressed: () { updateUserInfo(); },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80.0)
                      ),
                      elevation: 0.0,
                      padding: EdgeInsets.all(0.0),
                      child: Ink(
                        child: Container(
                          constraints: BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                          alignment: Alignment.center,
                          child: Text("Aggiorna info",
                            style: TextStyle(color: Colors.white, fontSize: 18.0),
                          ),
                        ),
                      ),
                      color: Colors.blue,
                    ),
                  ),
                  SizedBox(height: 20),
                  Visibility(
                    child: CircularProgressIndicator(),
                    visible: state.usersState.isLoading,
                  ),
                  SizedBox(height: 80),
                  Container(
                    width: 300.00,
                    child: RaisedButton(
                      onPressed: () { ReduxStore.store.dispatch(LogoutAction()); },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80.0)
                      ),
                      elevation: 0.0,
                      padding: EdgeInsets.all(0.0),
                      child: Ink(
                        child: Container(
                          constraints: BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                          alignment: Alignment.center,
                          child: Text("Esci",
                            style: TextStyle(color: Colors.white, fontSize: 18.0),
                          ),
                        ),
                      ),
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      }
    );
  }
}