import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/models/Post.dart';
import 'package:flutter_app/redux/common/common_actions.dart';
import 'package:flutter_app/redux/posts/post_actions.dart';
import 'package:flutter_app/redux/posts/post_states.dart';
import 'package:flutter_app/redux/state.dart';
import 'package:flutter_app/redux/store.dart';
import 'package:flutter_app/router.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:easy_localization/easy_localization.dart';

class PostsScreen extends StatefulWidget {
  @override
  _PostsScreenState createState() => _PostsScreenState();
}

class _PostsScreenState extends State<PostsScreen> {

  final FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    ReduxStore.store.dispatch(GetPostsAction());
  }

  void _onTap(Post post) {
    Navigator.pushNamed(context, AppRouter.detailsRoute, arguments: post);
    _focusNode.unfocus();
  }

  void _onEdit(Post post) =>
      Navigator.pushNamed(context, AppRouter.newPost, arguments: post);

  void _onLike(Post post) =>
      ReduxStore.store.dispatch(LikePostAction(post));

  void _onSearchPost(String query) =>
      ReduxStore.store.dispatch(SearchQueryAction(query));

  Future<Null> _refresh() async =>
      ReduxStore.store.dispatch(GetPostsAction());

  _confirmDelete() {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Eliminare il post?'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Questo post verrà eliminato.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('No'),
              onPressed: () => Navigator.pop(context, false),
            ),
            TextButton(
              child: Text('Si'),
              onPressed: () => Navigator.pop(context, true),
            ),
          ],
        );
      },
    );
  }

  Widget getRow(Post post) {
    var dateTime = DateTime.fromMillisecondsSinceEpoch(post.datetime);
    String date = DateFormat("HH:mm").format(dateTime);

    return Container(
      child: new InkWell(
        onTap: () => _onTap(post),
        child: new Card(
          color: Colors.white70,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                children: [
                  Expanded(
                    child: new ListTile(
                      title: new Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                        child: Text(post.title, style: TextStyle(fontSize: 20))
                      ),
                      subtitle: new Text("Pubblicato alle $date"),
                    ),
                  ),
                  Container(
                    child: ButtonBar(
                      children: <Widget>[
                        new FlatButton(
                          child: const Text('Modifica'),
                          onPressed: () => _onEdit(post),
                        ),
                      ],
                    ),
                  ),
                  Icon(Icons.arrow_back_ios_rounded)
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                        padding: const EdgeInsets.all(16),
                        child: Text("${post.comments.length} commmenti")
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      IconButton(
                        onPressed: () => { _onLike(post) },
                        icon: Icon(
                          Icons.favorite,
                          color: post.isLiked ? Colors.red : Colors.grey
                        ),
                      ),
                    ]
                  ),
                ],
              ),
            ],
          ),
        ),
      )
    );
  }

  Widget slideLeftBackground() {
    return Card(
      color: Colors.red,
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Icon(
              Icons.delete,
              color: Colors.white,
            ),
            Text(
              " Cancella",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.right,
            ),
            SizedBox(
              width: 20,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, PostsState>(
      distinct: true,
      converter: (store) => store.state.postsState,
      builder: (context, state) {
        return Scaffold(
            body: Container(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      onChanged: (query) { _onSearchPost(query); },
                      focusNode: _focusNode,
                      decoration: InputDecoration(
                        hintText: "Cerca post tramite il titolo",
                        prefixIcon: Icon(Icons.search),
                      ),
                    ),
                  ),
                  Center(
                    child: Visibility(
                      child: Padding(
                        padding: const EdgeInsets.all(16),
                        child: CircularProgressIndicator()
                      ),
                      visible: state.isLoading,
                    )
                  ),
                  Center(
                    child: Visibility(
                      child: Padding(
                        padding: const EdgeInsets.all(16),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.emoji_people_sharp, size: 50),
                            SizedBox(height: 20),
                            Text("Nessun post trovato",
                              style: TextStyle(fontSize: 18),
                            ),
                          ]
                        )
                      ),
                      visible: !state.isLoading && state.posts.isEmpty,
                    )
                  ),
                  Expanded(
                    child: RefreshIndicator(
                      onRefresh: _refresh,
                      child: ListView.builder(
                        itemCount: state.posts.length,
                        itemBuilder: (context, index) {
                          final post = state.posts[index];
                          return Dismissible(
                            key: Key(post.id),
                            direction: DismissDirection.endToStart,
                            background: slideLeftBackground(),
                            confirmDismiss: (direction) => _confirmDelete(),
                            onDismissed: (direction) => ReduxStore.store.dispatch(DeletePostAction(post)),
                            child: getRow(post),
                          );
                        },
                      )
                    )
                  )
                ]
              )
          )
        );
      }
    );
  }
}