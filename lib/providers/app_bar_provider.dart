import 'package:flutter/foundation.dart';

class AppBarProvider extends ChangeNotifier {
  String _title;

  String get title => _title;

  set title(String title) {
    _title = title;

    notifyListeners();
  }
}
