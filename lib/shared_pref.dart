import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

import 'models/User.dart';

class SharedPref {

  Future<bool> setUser(LocalUser user) async {
    SharedPreferences _storage = await SharedPreferences.getInstance();
    await _storage.setString('user', json.encode(user.toJson()));
    return user != null;
  }

  getUser() async {
    SharedPreferences _storage = await SharedPreferences.getInstance();
    return json.decode(_storage.getString('user'));
  }

  logout() async {
    SharedPreferences _storage = await SharedPreferences.getInstance();
    _storage.setString('user', null);
  }
}