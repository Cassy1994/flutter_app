import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_app/models/Comment.dart';
import 'package:flutter_app/models/Post.dart';
import 'package:flutter_app/models/User.dart';
import 'package:flutter_app/redux/posts/post_states.dart';
import 'package:flutter_app/redux/users/user_states.dart';
import 'package:flutter_app/shared_pref.dart';

class Rest {
  static final Rest _instance = Rest._internal();

  static FirebaseAuth _auth;
  static SharedPref _sharedPref;

  factory Rest() {
    if (_instance == null) {
      return Rest._internal();
    }
    return _instance;
  }

  Rest._internal() {
    _auth = FirebaseAuth.instance;
    _sharedPref = SharedPref();
    FirebaseFirestore.instance;
  }

  Future<UserState> login(String email, String password) async {
    String message;
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(email: email, password: password);
      final LocalUser localUser = LocalUser(userCredential.user.uid, userCredential.user.email);
      if (await _sharedPref.setUser(localUser)) {
        return UserState(
          isLoading: false,
          isAuthenticated: true,
          isError: false,
          user: localUser,
        );
      }
    }
    on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        message = 'Nessun utente trovato';
      }
      else if (e.code == 'wrong-password') {
        message = 'Password errata';
      }
    }
    return UserState(isLoading: false, isError: true, errorMessage: message);
  }

  Future<LocalUser> updateUserInfo(String email, String displayName, String phoneNumber) async {
    await _auth.currentUser.updateProfile(displayName: displayName);
    await _auth.currentUser.reload();
    User user = _auth.currentUser;
    LocalUser localUser = LocalUser.fromJson(await _sharedPref.getUser());
    if (localUser.email != "") email = user.email;
    if (localUser.displayName != "") localUser.displayName = user.displayName;
    if (localUser.phoneNumber != "") localUser.phoneNumber = user.phoneNumber;
    await _sharedPref.setUser(localUser);
    return localUser;
  }

  Future<PostsState> getPosts({String id}) async {
    List<Post> posts = new List();
    final fetchPosts = await loadFromFirebase();
    if (id == null) {
      fetchPosts.docs.forEach((element) {
        posts..add(Post.fromJson(element.id, element.data()));
      });
      return PostsState(posts: posts, isLoading: false);
    }
    else {
      fetchPosts.docs.forEach((element) {
        if (element.id == id) return PostsState(post: Post.fromJson(element.id, element.data()), isLoading: false);
      });
    }
  }

  Future<Post> newPost(String title, String body) async {
    final Post post = new Post(title: title, body: body);
    await FirebaseFirestore.instance.collection('posts').doc().set(post.toJson());
    return post;
  }

  Future<Post> newComment(Post post, Comment comment) async {
    post.comments..add(comment);
    await FirebaseFirestore.instance.collection('posts').doc(post.id).update(post.toJson());
    return post;
  }

  Future<Post> getLastPost() async {
    final lastPost = await FirebaseFirestore.instance.collection('posts').orderBy("date", descending: false).snapshots().first;
    return Post.fromJson(lastPost.docs.last.id, lastPost.docs.last.data());
  }

  Future<Post> editPost(Post post) async {
    await FirebaseFirestore.instance.collection('posts').doc(post.id).set(post.toJson());
    return post;
  }

  Future<Post> likePost(Post post) async {
    post.isLiked = !post.isLiked;
    await FirebaseFirestore.instance.collection('posts').doc(post.id).update(post.toJson());
    return post;
  }

  Future<Post> likeComment(Post post) async {
    await FirebaseFirestore.instance.collection('posts').doc(post.id).update(post.toJson());
    return post;
  }

  Future<Post> deletePost(Post post) async {
    await FirebaseFirestore.instance.collection('posts').doc(post.id).delete();
    return post;
  }

  Future<Post> deleteComment(Post post) async {
    await FirebaseFirestore.instance.collection('posts').doc(post.id).update(post.toJson());
    return post;
  }

  Future<QuerySnapshot> loadFromFirebase() {
    return FirebaseFirestore.instance.collection('posts').snapshots().first;
  }

  Future<LocalUser> getUser() async {
    var json = await _sharedPref.getUser();
    return LocalUser.fromJson(json);
  }

  Future<UserState> logout() async {
    _sharedPref.logout();
    return UserState(isLoading: false, isAuthenticated: false);
  }
}