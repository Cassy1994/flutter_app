import 'package:flutter_app/redux/posts/post_actions.dart';
import 'package:flutter_app/redux/posts/post_states.dart';
import 'package:redux/redux.dart';

final postReducer = combineReducers<PostsState>([
  TypedReducer<PostsState, SetPostsStateAction>(_getPostAction),
]);

PostsState _getPostAction(PostsState state, SetPostsStateAction action) {
  final payload = action.postsState;
  return state.copyWith(
    isError: payload.isError,
    isLoading: payload.isLoading,
    isNew: payload.isNew,
    isEdited: payload.isEdited,
    post: payload.post,
    posts: payload.posts,
  );
}