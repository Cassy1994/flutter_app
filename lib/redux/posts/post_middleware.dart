import 'package:flutter_app/api/rest.dart';
import 'package:flutter_app/models/Post.dart';
import 'package:flutter_app/redux/posts/post_actions.dart';
import 'package:flutter_app/redux/posts/post_states.dart';
import 'package:flutter_app/redux/state.dart';
import 'package:redux/redux.dart';

List<Middleware<AppState>> postMiddleware() {
  final fetchPosts = _fetchPosts();
  final getPost = _getPost();
  final newPost = _newPost();
  final newComment = _newComment();
  final editPost = _editPost();
  final editComment = _editComment();
  final likePost = _likePost();
  final likeComment = _likeComment();
  final deleteComment = _deleteComment();
  final deletePost = _deletePost();

  return [
    TypedMiddleware<AppState, GetPostsAction>(fetchPosts),
    TypedMiddleware<AppState, GetPostAction>(getPost),
    TypedMiddleware<AppState, NewPostAction>(newPost),
    TypedMiddleware<AppState, NewCommentAction>(newComment),
    TypedMiddleware<AppState, EditPostAction>(editPost),
    TypedMiddleware<AppState, EditCommentAction>(editComment),
    TypedMiddleware<AppState, LikePostAction>(likePost),
    TypedMiddleware<AppState, LikeCommentAction>(likeComment),
    TypedMiddleware<AppState, DeleteCommentAction>(deleteComment),
    TypedMiddleware<AppState, DeletePostAction>(deletePost),
  ];
}

Middleware<AppState> _fetchPosts() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    next(SetPostsStateAction(PostsState(isLoading: true)));
    PostsState postsState = await Rest().getPosts();
    next(SetPostsStateAction(postsState));
  };
}

Middleware<AppState> _getPost() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    if (action is GetPostAction) {
      var posts = store.state.postsState.posts;
      Post post = posts[posts.indexWhere((element) => element.id == action.id)];
      next(SetPostsStateAction(PostsState(post: post)));
    }
  };
}

Middleware<AppState> _newPost() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    next(SetPostsStateAction(PostsState(isLoading: true, isNew: false)));
    if (action is NewPostAction) {
      Post post = await Rest().newPost(action.title, action.body);
      next(SetPostsStateAction(PostsState(posts: store.state.postsState.posts..add(post), isLoading: false, isNew: true)));
    }
  };
}

Middleware<AppState> _newComment() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    if (action is NewCommentAction) {
      Post newPost = await Rest().newComment(action.post, action.comment);
      next(SetPostsStateAction(PostsState(post: newPost)));
    }
  };
}

Middleware<AppState> _editPost() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    next(SetPostsStateAction(PostsState(isEdited: false)));
    if (action is EditPostAction) {
      var posts = store.state.postsState.posts;
      Post newPost = await Rest().editPost(Post(id: action.id, title: action.title, body: action.body));
      posts[posts.indexWhere((element) => element.id == action.id)] = newPost;
      next(SetPostsStateAction(PostsState(posts: posts, isEdited: true)));
    }
  };
}

Middleware<AppState> _likePost() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    if (action is LikePostAction) {
      var posts = store.state.postsState.posts;
      Post newPost = await Rest().likePost(action.post);
      posts[posts.indexWhere((element) => element.id == action.post.id)] = newPost;
      next(SetPostsStateAction(PostsState(posts: posts)));
    }
  };
}

Middleware<AppState> _likeComment() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    if (action is LikeCommentAction) {
      var post = action.post;
      var index = action.index;
      post.comments[index].isLiked = !post.comments[index].isLiked;
      Post newPost = await Rest().likeComment(action.post);
      next(SetPostsStateAction(PostsState(post: newPost)));
    }
  };
}

Middleware<AppState> _editComment() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    if (action is EditCommentAction) {
      Post post = await Rest().editPost(action.post);
      next(SetPostsStateAction(PostsState(post: post)));
    }
  };
}

Middleware<AppState> _deleteComment() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    if (action is DeleteCommentAction) {
      var post = action.post;
      var index = action.index;
      post.comments.removeAt(index);
      Post newPost = await Rest().deleteComment(action.post);
      next(SetPostsStateAction(PostsState(post: newPost)));
    }
  };
}

Middleware<AppState> _deletePost() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    if (action is DeletePostAction) {
      await Rest().deletePost(action.post);
      next(SetPostsStateAction(PostsState(posts: store.state.postsState.posts..remove(action.post), isLoading: false)));
    }
  };
}