import 'package:flutter_app/models/Post.dart';
import 'package:meta/meta.dart';

@immutable
class PostsState {

  final bool isError;
  final bool isLoading;
  final bool isNew;
  final bool isEdited;
  final Post post;
  final List<Post> posts;

  PostsState({
    this.isError,
    this.isLoading,
    this.isNew,
    this.isEdited,
    this.post,
    this.posts,
  });

  factory PostsState.initial() => PostsState(
    isLoading: false,
    isError: false,
    isNew: false,
    isEdited: false,
    post: null,
    posts: const [],
  );

  PostsState copyWith({
    @required bool isError,
    @required bool isLoading,
    @required bool isNew,
    @required bool isEdited,
    @required Post post,
    @required List<Post> posts,
  }) {
    return PostsState(
      isError: isError ?? this.isError,
      isLoading: isLoading ?? this.isLoading,
      isNew: isNew ?? this.isNew,
      isEdited: isEdited ?? this.isEdited,
      post: post ?? this.post,
      posts: posts ?? this.posts,
    );
  }
}