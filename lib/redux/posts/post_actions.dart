import 'package:flutter_app/models/Comment.dart';
import 'package:flutter_app/models/Post.dart';
import 'package:flutter_app/redux/posts/post_states.dart';

class GetPostsAction { }

class GetPostAction {
  final String id;
  GetPostAction(this.id);
}

class NewPostAction {
  final String title;
  final String body;
  NewPostAction(this.title, this.body);
}

class EditPostAction {
  final String id;
  final String title;
  final String body;
  EditPostAction(this.id, this.title, this.body);
}

class LikePostAction {
  final Post post;
  LikePostAction(this.post);
}

class LikeCommentAction {
  final Post post;
  final int index;
  LikeCommentAction(this.post, this.index);
}

class DeleteCommentAction {
  final Post post;
  final int index;
  DeleteCommentAction(this.post, this.index);
}

class DeletePostAction {
  final Post post;
  DeletePostAction(this.post);
}

class NewCommentAction {
  final Post post;
  final Comment comment;
  NewCommentAction(this.post, this.comment);
}

class EditCommentAction {
  final Post post;
  EditCommentAction(this.post);
}

class SetPostsStateAction {
  final PostsState postsState;
  SetPostsStateAction(this.postsState);
}