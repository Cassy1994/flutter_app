import 'package:flutter_app/redux/users/user_states.dart';

class LoginAction {
  final String email;
  final String password;
  LoginAction(this.email, this.password);
}
class UpdateUserInfoAction {
  String email;
  String displayName;
  String phoneNumber;
  UpdateUserInfoAction(this.email, this.displayName, this.phoneNumber);
}

class UserIsLoggedAction { }

class LogoutAction { }

class UserCompletedAction {
  final UserState usersState;
  UserCompletedAction(this.usersState);
}