import 'package:flutter/material.dart';
import 'package:flutter_app/models/User.dart';
import 'package:meta/meta.dart';

@immutable
class UserState {

  final bool isError;
  final bool isLoading;
  final bool isAuthenticated;
  final String errorMessage;
  final LocalUser user;

  UserState({
    this.isError,
    this.isLoading,
    this.isAuthenticated,
    this.errorMessage,
    this.user,
  });

  factory UserState.initial() => UserState(
    isLoading: false,
    isError: false,
    isAuthenticated: false,
    errorMessage: "",
    user: null,
  );

  UserState copyWith({
    @required bool isError,
    @required bool isLoading,
    @required bool isAuthenticated,
    @required String errorMessage,
    @required LocalUser user,
  }) {
    return UserState(
      isError: isError ?? this.isError,
      isLoading: isLoading ?? this.isLoading,
      isAuthenticated: isAuthenticated ?? this.isAuthenticated,
      errorMessage: errorMessage ?? this.errorMessage,
      user: user ?? this.user,
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserState &&
          runtimeType == other.runtimeType &&
          isError == other.isError &&
          isLoading == other.isLoading &&
          isAuthenticated == other.isAuthenticated &&
          errorMessage == other.errorMessage &&
          user == other.user;

  @override
  int get hashCode =>
      isError.hashCode ^
      isLoading.hashCode ^
      isAuthenticated.hashCode ^
      errorMessage.hashCode ^
      user.hashCode;

  @override
  String toString() {
    return 'UserState{isError: $isError, isLoading: $isLoading, isAuthenticated: $isAuthenticated, errorMessage: $errorMessage, user: $user}';
  }
}