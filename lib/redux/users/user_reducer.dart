import 'package:flutter_app/redux/users/user_actions.dart';
import 'package:flutter_app/redux/users/user_states.dart';
import 'package:redux/redux.dart';

final userReducer = combineReducers<UserState>([
  TypedReducer<UserState, UserCompletedAction>(_userCompleted),
]);

UserState _userCompleted(UserState state, UserCompletedAction action) {
  final payload = action.usersState;
  return state.copyWith(
    isError: payload.isError,
    isLoading: payload.isLoading,
    isAuthenticated: payload.isAuthenticated,
    errorMessage: payload.errorMessage,
    user: payload.user,
  );
}