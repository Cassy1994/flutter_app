import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_app/api/rest.dart';
import 'package:flutter_app/models/User.dart';
import 'package:flutter_app/redux/state.dart';
import 'package:flutter_app/redux/users/user_actions.dart';
import 'package:flutter_app/redux/users/user_states.dart';
import 'package:redux/redux.dart';

List<Middleware<AppState>> userMiddleware() {

  final login = _login();
  final updateUserInfo = _updateUserInfo();
  final userIsLogged = _userIsLogged();
  final logout = _logout();

  return [
    TypedMiddleware<AppState, LoginAction>(login),
    TypedMiddleware<AppState, UpdateUserInfoAction>(updateUserInfo),
    TypedMiddleware<AppState, UserIsLoggedAction>(userIsLogged),
    TypedMiddleware<AppState, LogoutAction>(logout),
  ];
}

Middleware<AppState> _login() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    next(UserCompletedAction(UserState(isLoading: true)));
    if (action is LoginAction) {
      UserState userState = await Rest().login(action.email, action.password);
      next(UserCompletedAction(userState));
    }
  };
}

Middleware<AppState> _updateUserInfo() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    next(UserCompletedAction(UserState(isLoading: true)));
    if (action is UpdateUserInfoAction) {
      LocalUser localUser = await Rest().updateUserInfo(action.email, action.displayName, action.phoneNumber);
      next(UserCompletedAction(UserState(user: localUser, isLoading: false)));
    }
  };
}

Middleware<AppState> _userIsLogged() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    LocalUser localUser = await Rest().getUser();
    next(UserCompletedAction(UserState(user: localUser, isAuthenticated: localUser != null)));
  };
}

Middleware<AppState> _logout() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    UserState userState = await Rest().logout();
    next(UserCompletedAction(userState));
  };
}

