import 'package:flutter_app/redux/common/common_states.dart';
import 'package:flutter_app/redux/common/common_actions.dart';
import 'package:redux/redux.dart';

final commonReducer = combineReducers<CommonStates>([
  TypedReducer<CommonStates, UpdateAppBarCompletedAction>(_appbarCompleted),
]);

CommonStates _appbarCompleted(CommonStates state, UpdateAppBarCompletedAction action) {
  final payload = action.appBarState;
  return state.copyWith(
    appBarTitle: payload.appBarTitle,
  );
}