import 'package:flutter/cupertino.dart';

@immutable
class CommonStates {

  final String appBarTitle;

  CommonStates({
    this.appBarTitle,
  });

  factory CommonStates.initial() => CommonStates(
    appBarTitle: "Home",
  );

  CommonStates copyWith({
    @required String appBarTitle,
  }) {
    return CommonStates(
      appBarTitle: appBarTitle ?? this.appBarTitle,
    );
  }

  @override
  String toString() {
    return 'CommonStates{appBarTitle: $appBarTitle}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CommonStates &&
          runtimeType == other.runtimeType &&
          appBarTitle == other.appBarTitle;

  @override
  int get hashCode => appBarTitle.hashCode;
}