import 'package:flutter_app/redux/common/common_states.dart';

class AppBarAction {
  final CommonStates appBarState;
  AppBarAction(this.appBarState);
}

class UpdateAppBarAction extends AppBarAction {
  UpdateAppBarAction(CommonStates appBarState) : super(appBarState);
}

class UpdateAppBarCompletedAction extends AppBarAction{
  UpdateAppBarCompletedAction(CommonStates appBarState) : super(appBarState);
}

class SearchQueryAction {
  String query;
  SearchQueryAction(this.query);
}