import 'package:flutter_app/api/rest.dart';
import 'package:flutter_app/redux/common/common_states.dart';
import 'package:flutter_app/redux/common/common_actions.dart';
import 'package:flutter_app/redux/posts/post_actions.dart';
import 'package:flutter_app/redux/posts/post_states.dart';
import 'package:flutter_app/redux/state.dart';
import 'package:redux/redux.dart';

List<Middleware<AppState>> commonMiddleware() {

  final updateAppBar = _updateAppBar();
  final searchQuery = _searchQuery();

  return [
    TypedMiddleware<AppState, UpdateAppBarAction>(updateAppBar),
    TypedMiddleware<AppState, SearchQueryAction>(searchQuery),
  ];
}

Middleware<AppState> _updateAppBar() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    if (action is UpdateAppBarAction) {
      next(UpdateAppBarCompletedAction(CommonStates(appBarTitle: action.appBarState.appBarTitle)));
    }
  };
}

Middleware<AppState> _searchQuery() {
  return (Store<AppState> store, action, NextDispatcher next) async {
    if (action is SearchQueryAction) {
      PostsState postsState = await Rest().getPosts();
      var results = postsState.posts.where((element) => element.title.toLowerCase().contains(action.query)).toList();
      results.forEach((element) { print(element.title); });
      next(SetPostsStateAction(PostsState(posts: results)));
    }
  };
}
