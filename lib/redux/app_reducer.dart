import 'package:flutter_app/redux/common/common_reducer.dart';
import 'package:flutter_app/redux/posts/post_reducer.dart';
import 'package:flutter_app/redux/state.dart';
import 'package:flutter_app/redux/users/user_reducer.dart';

AppState appReducer(AppState state, dynamic action) =>
  new AppState(
    usersState: userReducer(state.usersState, action),
    postsState: postReducer(state.postsState, action),
    commonStates: commonReducer(state.commonStates, action)
  );