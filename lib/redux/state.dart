import 'package:flutter/cupertino.dart';
import 'package:flutter_app/redux/common/common_states.dart';
import 'package:flutter_app/redux/posts/post_states.dart';
import 'package:flutter_app/redux/users/user_states.dart';

@immutable
class AppState {
  final PostsState postsState;
  final UserState usersState;
  final CommonStates commonStates;
  // Altri stati

  AppState({
    @required this.postsState,
    @required this.usersState,
    @required this.commonStates,
    // ...
  });

  @override
  String toString() {
    return 'AppState{postsState: $postsState, usersState: $usersState, commonStates: $commonStates}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppState &&
          runtimeType == other.runtimeType &&
          postsState == other.postsState &&
          usersState == other.usersState &&
          commonStates == other.commonStates;

  @override
  int get hashCode =>
      postsState.hashCode ^ usersState.hashCode ^ commonStates.hashCode;

  AppState copyWith({
    PostsState postsState,
    UserState usersState,
    CommonStates appBarState,
    // ...
  }) {
    return AppState(
      postsState: postsState ?? this.postsState,
      usersState: usersState ?? this.usersState,
      commonStates: commonStates ?? this.commonStates,
      // ...
    );
  }
}