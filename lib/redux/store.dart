import 'package:flutter_app/redux/common/common_states.dart';
import 'package:flutter_app/redux/posts/post_middleware.dart';
import 'package:flutter_app/redux/app_reducer.dart';
import 'package:flutter_app/redux/posts/post_states.dart';
import 'package:flutter_app/redux/state.dart';
import 'package:flutter_app/redux/users/user_middleware.dart';
import 'package:flutter_app/redux/users/user_states.dart';
import 'package:redux/redux.dart';

import 'common/common_middleware.dart';

class ReduxStore {
  static Store<AppState> _store;

  static Store<AppState> get store {
    if (_store == null) {
      throw Exception("store is not initialized");
    }
    else {
      return _store;
    }
  }

  static Future<void> init() async {

    final postsStateInitial = PostsState.initial();
    final userStateInitial = UserState.initial();
    final commonStateInitial = CommonStates.initial();

    _store = Store<AppState>(
      appReducer,
      middleware: []
        ..addAll(commonMiddleware())
        ..addAll(userMiddleware())
        ..addAll(postMiddleware()),
      initialState: AppState(
          postsState: postsStateInitial,
          usersState: userStateInitial,
          commonStates: commonStateInitial
      )
    );
  }
}
