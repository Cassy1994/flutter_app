import 'package:flutter_app/redux/state.dart';
import 'package:flutter_app/redux/users/user_actions.dart';
import 'package:flutter_app/screens/login_screen.dart';
import 'package:flutter_app/screens/main_screen.dart';
import 'package:flutter_app/screens/new_edit_post.dart';
import 'package:flutter_app/screens/post_details_screen.dart';
import 'package:flutter_redux/flutter_redux.dart';

class AppRouter {
  static final loginRoute = '/login';
  static final mainRoute = '/main';
  static final detailsRoute = '/post';
  static final newPost = '/new_post';

  static routes() {
    return {
      mainRoute: (context) {
        return StoreConnector<AppState, bool>(
          converter: (store) => store.state.usersState.isAuthenticated,
          onInit: (store) => store.dispatch(UserIsLoggedAction()),
          builder: (_, isAuthenticated) {
            if (isAuthenticated) {
              return MainScreen();
            }
            return LoginScreen();
          },
        );
      },
      loginRoute: (context) => LoginScreen(),
      detailsRoute: (context) => PostDetailsScreen(),
      newPost: (context) => NewEditPostScreen()
    };
  }
}
