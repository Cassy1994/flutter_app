import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/providers/app_bar_provider.dart';
import 'package:flutter_app/redux/state.dart';
import 'package:flutter_app/redux/store.dart';
import 'package:flutter_app/router.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await ReduxStore.init();

  runApp(EasyLocalization(
    supportedLocales: [Locale('en'), Locale('it')],
    path: 'assets/translations',
    fallbackLocale: Locale('it'),
    child: MultiProvider(providers: [
        ChangeNotifierProvider(create: (_) => AppBarProvider()),
      ], child: MyApp()
    )
  ));
}

class MyApp extends StatefulWidget {
  @override
  createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: ReduxStore.store,
      child: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      initialRoute: AppRouter.mainRoute,
      routes: AppRouter.routes()
    );
  }
}