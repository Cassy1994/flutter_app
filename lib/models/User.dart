class LocalUser {
  String uid;
  String email;
  String displayName;
  String phoneNumber;

  LocalUser(String uid, String email, {String displayName, String phoneNumber}) {
    this.uid = uid;
    this.email = email;
    this.displayName = displayName;
    this.phoneNumber = phoneNumber;
  }

  void setEmail(String email) => this.email = email;

  void setDisplayName(String displayName) => this.displayName = displayName;

  void setPhoneNumber(String phoneNumber) => this.phoneNumber = phoneNumber;

  LocalUser.fromJson(Map<String, dynamic> json)
      : uid = json['uid'],
        email = json['email'],
        displayName = json['displayName'],
        phoneNumber = json['phoneNumber'];

  Map<String, dynamic> toJson() =>
    {
      'uid': uid,
      'email': email,
      'displayName': displayName,
      'phoneNumber': phoneNumber
    };
}
