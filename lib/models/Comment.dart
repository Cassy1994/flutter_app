class Comment {
  String body;
  String id;
  bool isLiked = false;

  Comment(this.body, {this.id});

  Comment.fromJson(dynamic json) {
    body = json["body"];
    id = json["id"];
    isLiked = json['isLiked'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["body"] = body;
    map["id"] = id;
    map["isLiked"] = isLiked;
    return map;
  }

  @override
  String toString() {
    return 'Comment{body: $body, id: $id}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Comment &&
          runtimeType == other.runtimeType &&
          body == other.body;

  @override
  int get hashCode => body.hashCode;
}