import 'package:flutter_app/models/Comment.dart';

class Post {
  String id;
  String userId;
  String title;
  String body;
  bool isLiked = false;
  List<Comment> comments = new List();
  var datetime = DateTime.now().millisecondsSinceEpoch;

  Post({ this.title, this.body, this.id, this.userId });

  Post.fromJson(String id, Map<dynamic, dynamic> json) {
    if (json == null) return;
    this.id = id;
    this.userId = json['userId'];
    this.title = json['title'];
    this.body = json['body'];
    this.isLiked = json['isLiked'];
    this.datetime = json['datetime'];
    if (json["comments"] != null) {
      comments = [];
      json["comments"].forEach((v) {
        comments.add(Comment.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["userId"] = userId;
    map["title"] = title;
    map["body"] = body;
    map["isLiked"] = isLiked;
    map["datetime"] = datetime;
    if (comments != null) {
      map["comments"] = comments.map((v) => v.toJson()).toList();
    }
    return map;
  }

  @override
  String toString() {
    return 'Post{id: $id, userId: $userId, title: $title, body: $body, isLiked: $isLiked, comments: $comments, datetime: $datetime}';
  }
}